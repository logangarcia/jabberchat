# JabberGPT

JabberGPT is an Jabber/XMPP chatbot powered by OpenAI. It allows you to interact with an AI-powered chatbot via XMPP.

## Getting Started

### Prerequisites
- Python 3
- OpenAI API key
- XMPP bot account

### Installation
1. Clone the JabberGPT repository:

   ```bash
   git clone https://gitlab.com/logangarcia/jabbergpt.git
   cd jabbergpt
   ```

2. Install the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

### Configuration
1. Rename the `config.sample.py` file to `config.py`.

2. Open `config.py` and configure the following settings:
   - `OPENAI_API_KEY`: Your OpenAI API key.
   - `XMPP_HOST`: The hostname of your XMPP server.
   - `XMPP_PORT`: The port number of your XMPP server.
   - `XMPP_USERNAME`: The username of the chatbot.
   - `XMPP_PASSWORD`: The password of the chatbot.
   - `XMPP_ROOMS`: A list of XMPP rooms to join.
   - `XMPP_NICKNAME`: The nickname to use in XMPP rooms.
   - `GPT3_CHAT_MODEL`: The GPT-3 model ID for chat conversations.

### Usage
1. Run the JabberGPT chatbot:

   ```bash
   python jabbergpt.py
   ```

2. Once the chatbot is running, it will connect to the XMPP server and join the specified rooms.

3. Start interacting with the chatbot via XMPP messages or in the joined rooms.

## Contributing
Contributions are welcome! If you have any suggestions, bug reports, or feature requests, please open an issue or submit a pull request.

## License
JabberGPT is released under the [MIT License](https://opensource.org/licenses/MIT).
